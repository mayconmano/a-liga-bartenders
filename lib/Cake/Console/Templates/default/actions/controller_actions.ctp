<?php
/**
 * Bake Template for Controller action generation.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.actions
 * @since         CakePHP(tm) v 1.3
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

        public $js = array();
	public $css = array();
	

	public function beforeRender(){				
                $this->set('js', $this->js);
                $this->set('css', $this->css);
	}

/**
 * <?php echo $admin ?>index method
 *
 * @return void
 */     
        public function index() {
        
                $this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';        		
	}

/**
 * <?php echo $admin ?>lista method
 *
 * @return void
 */        
        public function lista(){
            $this->render(false, false);
            $this-><?php echo $currentModelName ?>->recursive = 1;
            $data = array();
            
            $search = "";

            if($_GET['search']['value'] != ""){
                $colunasArr = array();
                foreach($_GET['columns'] as $column){
                    if($column['name'] != ""){
                        $colunasArr[] = $column['name']." LIKE '%".$_GET['search']['value']."%'";
                    }
                }                
                $search = ' AND '.join(' OR ', $colunasArr);                                
            }
            
            $ordem = "";
            if($_GET['order'][0]['column'] != ""){
                $coluna = $_GET['columns'][$_GET['order'][0]['column']]['name'];
                $ordemTipo = $_GET['order'][0]['dir'];
                $ordem = " ORDER BY ".$coluna." ".$ordemTipo;                
            }  
            
            $d = $this-><?php echo $currentModelName ?>->find('all', array('conditions' => array('1=1 '.$search.' '.$ordem.' LIMIT '.$_GET['start'].','.$_GET['length'].'')));
            $dTotal = $this-><?php echo $currentModelName ?>->find('count');
            if(!empty($_GET['search']['value'])){
                $dTotalFiltred = $this-><?php echo $currentModelName ?>->find('count', array('conditions' => array('1=1 '.$search)));
            }else{
                $dTotalFiltred = $dTotal;
            }
            
            
            $data['data'] = array();
            foreach($d as $key => $v){                                  
                $data['data'][$key] = $v;
                $data['data'][$key]['DT_RowId'] = $v['<?php echo $currentModelName ?>']['id'];                
            }
            $data['recordsFiltered'] = $dTotalFiltred;
            $data['recordsTotal'] = $dTotal;
            echo json_encode($data);
                
        }

/**
 * <?php echo $admin ?>view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function <?php echo $admin ?>view($id = null) {
		if (!$this-><?php echo $currentModelName; ?>->exists($id)) {
			throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
		}
		$options = array('conditions' => array('<?php echo $currentModelName; ?>.' . $this-><?php echo $currentModelName; ?>->primaryKey => $id));
		$this->set('<?php echo $singularName; ?>', $this-><?php echo $currentModelName; ?>->find('first', $options));
	}

<?php $compact = array(); ?>
/**
 * <?php echo $admin ?>add method
 *
 * @return void
 */
	public function <?php echo $admin ?>add() {
                $this->css[] = '/js/jquery.icheck/skins/square/blue';

		$this->js[] = 'jquery.icheck/icheck.min';
        
		if ($this->request->is('post')) {
			$this-><?php echo $currentModelName; ?>->create();
			if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>				
                                $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Cadastrado com sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao cadastrar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
<?php else: ?>
				return $this->flash(__('$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Cadastrado com sucesso!</strong></div>'));'), array('action' => 'index'));
<?php endif; ?>
			}
		}
<?php
	foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
		foreach ($modelObj->{$assoc} as $associationName => $relation):
			if (!empty($associationName)):
				$otherModelName = $this->_modelName($associationName);
				$otherPluralName = $this->_pluralName($associationName);
				echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list', array('fields' => array('id', 'nome')));\n";
				$compact[] = "'{$otherPluralName}'";
			endif;
		endforeach;
	endforeach;
	if (!empty($compact)):
		echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
	endif;
?>
	}

<?php $compact = array(); ?>
/**
 * <?php echo $admin ?>edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function <?php echo $admin; ?>edit($id = null) {
                $this->css[] = '/js/jquery.icheck/skins/square/blue';

		$this->js[] = 'jquery.icheck/icheck.min';    
                
		if (!$this-><?php echo $currentModelName; ?>->exists($id)) {
			throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Editado com sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {				
                                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao editar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
<?php else: ?>
				return $this->flash(__('$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Editado com sucesso!</strong></div>'));'), array('action' => 'index'));
<?php endif; ?>
			}
		} else {
			$options = array('conditions' => array('<?php echo $currentModelName; ?>.' . $this-><?php echo $currentModelName; ?>->primaryKey => $id));
			$this->request->data = $this-><?php echo $currentModelName; ?>->find('first', $options);
		}
<?php
		foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
			foreach ($modelObj->{$assoc} as $associationName => $relation):
				if (!empty($associationName)):
					$otherModelName = $this->_modelName($associationName);
					$otherPluralName = $this->_pluralName($associationName);
					echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list', array('fields' => array('id', 'nome')));\n";
					$compact[] = "'{$otherPluralName}'";
				endif;
			endforeach;
		endforeach;
		if (!empty($compact)):
			echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
		endif;
	?>
	}

/**
 * <?php echo $admin ?>delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function <?php echo $admin; ?>delete($id = null) {
		$this-><?php echo $currentModelName; ?>->id = $id;
		if (!$this-><?php echo $currentModelName; ?>->exists()) {
			throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this-><?php echo $currentModelName; ?>->delete()) {
<?php if ($wannaUseSession): ?>			
                        $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Deletado com sucesso!</strong></div>'));
                        
		} else {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao deletar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
		}
		return $this->redirect(array('action' => 'index'));
<?php else: ?>
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Deletado com sucesso!</strong></div>'));
		} else {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao deletar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
		}
<?php endif; ?>
	}
        
 /**
 * <?php echo $admin ?>status method
 *
 * @param string $id
 * @param string $status
 * @return void
 */       
        public function status($id, $status) {

		$this->autoRender = false;

		$data['id'] = $id;
		$data['status'] = $status;

		if($this-><?php echo $currentModelName; ?>->save($data)) {
			if($status == 1) {
				$return['title'] = 'Sucesso';
				$return['text'] = '<?php echo $currentModelName; ?> ativa!';
				$return['class_name'] = 'success';
			}else{
				$return['title'] = 'Sucesso';
				$return['text'] = '<?php echo $currentModelName; ?> inativa!';
				$return['class_name'] = 'dark';
			}			
		}else{
			$return['title'] = 'Erro';
			$return['text'] = 'Tente mais tarde!';
			$return['class_name'] = 'red';
		}

		echo json_encode($return);
		
	}
