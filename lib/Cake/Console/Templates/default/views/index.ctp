<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="block-flat">
            <div class="header">							
                <h3><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h3>
            </div>
            <div class="content">
                <div class="table-responsive">
                    <table class="table table-bordered" id="datatable" >
                       
                        <tbody>
                            <?php /*                           
                            echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";                            
                            echo "\t<tr id='<?php echo h(\${$singularVar}['{$modelClass}']['id']); ?>'>\n";
                            foreach ($fields as $key => $field) {
                                $isKey = false;
                                if($field == 'status'){
                                    echo "<td><?php echo (\${$singularVar}['{$modelClass}']['status'] == 1 ? '<span class=\"label label-success inativo\" style=\"cursor: pointer;\">Ativo</span>' : '<span class=\"label label-default ativo\" style=\"cursor: pointer;\">Inativo</span>'); ?></td>";
                                }else{
                                
                                    if (!empty($associations['belongsTo'])) {
                                        foreach ($associations['belongsTo'] as $alias => $details) {
                                            if ($field === $details['foreignKey']) {
                                                $isKey = true;
                                                echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
                                                break;
                                            }
                                        }
                                    }
                                    if ($isKey !== true) {
                                        echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
                                    }     
                                }
                                                                                                
                            }                                                        
                            echo "\t\t<td>\n";                            
                            echo "\t\t</td>\n";
                            echo "\t</tr>\n";

                            echo "<?php endforeach; ?>\n";*/
                            ?>
                        </tbody>						


                    </table>							
                </div>
            </div>
        </div>				
    </div>
</div> 

<script>


    $(document).ready(function () {
        
        var columns = [];
        <?php foreach ($fields as $field): ?>    
            <?php $isKey = false; ?>    
            <?php if($field == 'status'){ ?>
                columns.push({
                    "title": "status",
                    "data": function(row){                    
                        if(row.<?= $modelClass; ?>.status == 1){
                            return '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>';
                        }else{
                            return '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>';
                        }                    
                    },
                    "name": "<?= $modelClass; ?>.status",
                });
            <?php }else{ ?>
                <?php
                    if(!empty($associations['belongsTo'])){
                        foreach ($associations['belongsTo'] as $alias => $details) {                            
                            if ($field === $details['foreignKey']) {
                                $isKey = true;
                 ?>
                         columns.push({
                             "title": "<?= $details['controller']; ?>",
                             "data": function(row){                                                                  
                                 return row.<?= $alias; ?>.nome;                                 
                             },
                             "name": "<?= $alias; ?>.nome"
                         });
                                
                 <?php 
                                break;
                            }
                        }
                    }
                    if(!strripos($field, '_id')){
                ?>
                columns.push({"title": "<?php echo $field; ?>", "data": "<?= $modelClass; ?>.<?php echo $field; ?>", "name": "<?= $modelClass; ?>.<?= $field ?>"});            
            <?php } } ?>            
        <?php endforeach; ?>
            columns.push({     
                    "title": "Ações",
                    "targets": -1,
                    "data": null,
                    "defaultContent": '<div class="btn-group"><button class="btn btn-default btn-md" type="button">Ações</button><button data-toggle="dropdown" class="btn btn-md btn-primary dropdown-toggle" type="button"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul role="menu" class="dropdown-menu pull-right"><li><a href="" class="editar">Editar</a></li><li><a href="" class="deletar">Deletar</a></li></ul></div>'
                });
        console.log(columns);
        $('#datatable').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "http://"+ window.location.host +"/gerenciador/<?php echo $pluralVar; ?>/lista",
            "columns": columns
            
        });
        
        $('.dataTables_length select').addClass('form-control');

        $(document).on('click', '.editar', function (e) {
            e.preventDefault();
            var id = $(this).parents().eq(4).attr('id');
            window.location = 'http://' + window.location.host +'/gerenciador/<?php echo $pluralVar; ?>/edit/' + id;
        });

        $(document).on('click', '.deletar', function (e) {
            e.preventDefault();
            var id = $(this).parents().eq(4).attr('id');
            window.location = 'http://' + window.location.host +'/gerenciador/<?php echo $pluralVar; ?>/delete/' + id;
        });

        $(document).on('click', '.ativo', function (e) {
            e.preventDefault();
            var id = $(this).parent().parent().attr('id');
            var elemt = $(this);
            $.ajax({
                url: 'http://' + window.location.host +'/gerenciador/<?php echo $pluralVar; ?>/status/' + id + '/1',
                dataType: 'json',
                success: function (data) {
                    elemt.removeClass('label-default ativo').addClass('label-success inativo').html('Ativo');
                    $.gritter.add({
                        title: data.title,
                        text: data.text,
                        class_name: data.class_name
                    });
                }
            });
        });

        $(document).on('click', '.inativo', function (e) {
            e.preventDefault();
            var id = $(this).parent().parent().attr('id');
            var elemt = $(this);
            $.ajax({
                url: 'http://' + window.location.host +'/gerenciador/<?php echo $pluralVar; ?>/status/' + id + '/0',
                dataType: 'json',
                success: function (data) {
                    elemt.removeClass('label-success inativo').addClass('label-default ativo').html('Inativo');
                    $.gritter.add({
                        title: data.title,
                        text: data.text,
                        class_name: data.class_name
                    });
                }
            });
        });

    });
</script>