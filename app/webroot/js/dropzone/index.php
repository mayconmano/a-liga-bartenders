<html>
 
<head>   
 
<!-- 1 -->
<link href="css/dropzone.css" type="text/css" rel="stylesheet" />
 
 <script src="jquery.js"></script>
<!-- 2 -->
<script src="dropzone.min.js"></script>
 
</head>
 
<body>
 
<!-- 3 -->
<!-- <form action="upload.php" class="dropzone" id="my-dropzone"></form> -->
 
<div class="dropzone" id="my-dropzone" style="width: 400px;"></div>

<ul id="fotos"></ul>

</body>

<script type="text/javascript">

	Dropzone.options.myDropzone = {
		url: 'upload.php',
		paramName: 'imagem',
		maxFiles: 4,
		dictRemoveFile: 'Remover',
		addRemoveLinks: true,		
	    init: function() {
	        thisDropzone = this;
	        
	        // $.get('upload.php', function(data) {
	 	        
	        //     $.each(data, function(key,value){
	                 
	        //         var mockFile = { name: value.name, size: value.size };	                 
	        //         thisDropzone.options.addedfile.call(thisDropzone, mockFile);	 
	        //         thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "uploads/"+value.name);
	                 
	        //     });
	             
	        // });	 


	        thisDropzone.on("maxfilesexceeded", function(file) { this.removeFile(file); });	  

	        
	        thisDropzone.on("success", function (file, responseText) {
	        	var html = '';
	            responseText = $.parseJSON(responseText);	            	            
	            equalName = false;
	            $.each(responseText, function(i, v){	            		            	
	            	$('#fotos li').each(function(){
	            		if($(this).children('input[name="nameFile[]"]').val() == v.nameFile)
	            			equalName = true;
	            	});	

	            	if(!equalName)
	            	{
		            	html += '<li>';
		            	html += '<input type="text" name="nameFile[]" value="'+v.nameFile+'">';
		            	html += '<input type="text" name="targetFile[]" value="'+v.targetFile+'">';
		            	html += '</li>';
		            }
	            }); 
	            $('#fotos').append(html);     
	        });	    

	        thisDropzone.on("removedfile", function (file) {

	            var name = file.name;  

	            $('#fotos li').each(function(){
	            	if($(this).children('input[name="nameFile[]"]').val() == name)
	            	{
	            		$(this).remove();
	            	}
	            });
	        });      

	    }
	   
	};
	

</script>
 
</html>