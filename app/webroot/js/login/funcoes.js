$(document).ready(function () {

    window.ParsleyValidator.setLocale('pt');

    var isValid = false;

    $('form').parsley().subscribe('parsley:form:validate', function (formInstance) {
        if (formInstance.isValid())
            isValid = true;
        else
            isValid = false;
    });

    $('#recuperaSenha').submit(function (e)
    {
        e.preventDefault();
        if (isValid) {
            var opts = {
                lines: 17, // The number of lines to draw
                length: 26, // The length of each line
                width: 13, // The line thickness
                radius: 27, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 40, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#000', // #rgb or #rrggbb or array of colors
                speed: 1.1, // Rounds per second
                trail: 43, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: '50%', // Top position relative to parent
                left: '50%' // Left position relative to parent
            };

            var dados = $(this).serialize();
            $.ajax({
                url: 'http://' + window.location.host + '/login/enviarRecuperacao',
                data: dados,
                type: 'POST',
                beforeSend: function ()
                {
                    var target = document.getElementById('foo');
                    var spinner = new Spinner(opts).spin(target);
                },
                success: function (data)
                {
                    var target = document.getElementById('foo');
                    target.innerHTML = '';

                    if (data == 'sucesso')
                    {
                        $('#sucesso').modal('show');
                    }
                    else if (data == 'erro')
                    {
                        $('#erro .modal-body').find('p').html('Problema ao enviar e-mail, por favor tente mais tarde!');
                        $('#erro').modal('show');
                    }
                    else if (data == 'email_nao_existe')
                    {
                        $('#erro .modal-body').find('p').html('Este e-mail não consta em nossa base de dados!');
                        $('#erro').modal('show');
                    }
                }
            });
        }
    });


    $('#alterarSenha').submit(function (e)
    {

        e.preventDefault();
        if (isValid) {
            var opts = {
                lines: 17, // The number of lines to draw
                length: 26, // The length of each line
                width: 13, // The line thickness
                radius: 27, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 40, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#000', // #rgb or #rrggbb or array of colors
                speed: 1.1, // Rounds per second
                trail: 43, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: '50%', // Top position relative to parent
                left: '50%' // Left position relative to parent
            };

            var dados = $(this).serialize();

            $.ajax({
                url: 'http://' + window.location.host + '/login/enviarAlteracao',
                data: dados,
                type: 'POST',
                beforeSend: function ()
                {
                    var target = document.getElementById('foo');
                    var spinner = new Spinner(opts).spin(target);
                },
                success: function (data)
                {
                    var target = document.getElementById('foo');
                    target.innerHTML = '';

                    if (data == 'sucesso')
                    {
                        $('#sucesso').modal('show');
                        $('#sucesso').on('hidden.bs.modal', function (e) {
                            window.location = 'http://' + window.location.host;
                        })
                    }
                    else if (data == 'erro_salvar')
                    {
                        $('#erro .modal-body').find('p').html('Problema ao alterar, por favor tente mais tarde!');
                        $('#erro').modal('show');
                    }
                    else if (data == 'erro')
                    {
                        $('#erro .modal-body').find('p').html('Problema ao enviar email, por favor tente mais tarde!');
                        $('#erro').modal('show');
                    }
                    else if (data == 'user_nao_existe')
                    {
                        $('#erro .modal-body').find('p').html('Usuário não existe em nosso banco de dados!');
                        $('#erro').modal('show');
                    }

                }
            });
        }
    });



});