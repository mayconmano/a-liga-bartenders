$(document).ready(function () {

    $('#datatable').dataTable({'iDisplayLength': -1, bFilter: false, bInfo: false, "bPaginate": false, "bSort": false});
    $('.dataTables_length select').addClass('form-control');

    $("#datatable tbody").sortable({
        update: function (event, ui) {
            ids = [];
            $('#datatable tbody tr').each(function (i, v) {
                ids.push($(this).attr('id'));
            });

            $.ajax({
                url: 'http://' + window.location.host + config_cidade + '/selfguiededs/order',
                type: 'post',
                dataType: 'json',
                data: {id: JSON.stringify(ids)},
                success: function (data) {
                    var html = '';
                    $.each(data, function (i, v) {
                        if (i % 2 == 0)
                            $class = "old";
                        else
                            $class = "even";
                        html += '<tr id="' + v['Poie']['id'] + '" class="' + $class + '">';
                        html += '<td>' + v['Poie']['ordem'] + '</td>';
                        html += '<td><a href="http://'+window.location.host+config_cidade+'/pois/editar/'+v['Poie']['id']+'">' + v['Poie']['nome_pt'] + '</a></td>';
                        html += '</tr>';
                    });
                    $('table tbody').html(html);
                }
            });
        }
    });

});