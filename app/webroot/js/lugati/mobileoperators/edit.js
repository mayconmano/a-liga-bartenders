$(document).ready(function(){

  $('#fileupload').fileupload({    
      url: config_cidade+'/mobileoperators/uploadHandlerEdit/'+$('input[name="id"]').val(),
      maxNumberOfFiles: 1
  });
  
  $('#fileupload').fileupload(
      'option',
      'redirect',
      window.location.href.replace(
          /\/[^\/]*$/,
          '/cors/result.html?%s'
      )
  );

  $('#fileupload').addClass('fileupload-processing');

  $.ajax({
      url: $('#fileupload').fileupload('option', 'url'),
      dataType: 'json',
      context: $('#fileupload')[0]
  }).always(function () {
      $(this).removeClass('fileupload-processing');
  }).done(function (result) {
      $(this).fileupload('option', 'done')
          .call(this, $.Event('done'), {result: result});
  });

});

function rawurlencode(str) {

  str = (str + '').toString();

  return encodeURIComponent(str)
    .replace(/!/g, '%21')
    .replace(/'/g, '%27')
    .replace(/\(/g, '%28')
    .replace(/\)/g, '%29')
    .replace(/\*/g, '%2A');
}