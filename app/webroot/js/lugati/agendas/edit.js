$(document).ready(function () {

    if ($('input[name="opcoes_link"]:checked').attr('id') == 'hiperlink')
    {
        $('#abre_hiperlink').show();
    }
    else if ($('input[name="opcoes_link"]:checked').attr('id') == 'noticias')
    {
        $('#abre_noticias').show();
    } else {
        $('input[name="interno"]').attr('checked', true);
    }

    window.ParsleyValidator.setLocale('pt');

    $('form').parsley();


    $('input[name="opcoes_link"]').on('ifChecked', function (event) {
        if ($(this).attr('id') == 'hiperlink')
        {
            $('#abre_noticias').hide();
            $('#abre_hiperlink').show();
            $('select[name="news_id"]').val(0);
            $('input[name="interno"]').attr('checked', false);
        } else if ($(this).attr('id') == 'noticias') {
            $('#abre_hiperlink').hide();
            $('#abre_noticias').show();
            $('input[name="link"]').val('');
            $('input[name="interno"]').attr('checked', false);
        } else {
            $('#abre_hiperlink').hide();
            $('#abre_noticias').hide();
            $('input[name="link"]').val('');
            $('select[name="news_id"]').val(0);
            $('input[name="interno"]').attr('checked', true);
        }
    });



    tinymce.init({
        selector: "#editor",
        width: 500,
        height: 300,
        toolbar: "insertfile undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview fullpage | forecolor backcolor",
    });

    $('#fileupload').fileupload({
        url: config_cidade + '/agendas/uploadHandlerEdit/' + $('input[name="id"]').val(),
        maxNumberOfFiles: 1
    });

    $('#fileupload').fileupload(
            'option',
            'redirect',
            window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                    )
            );


    $('#fileupload').addClass('fileupload-processing');

    $.ajax({
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
    });

    $('textarea[name="resumo"]').keypress(function (e) {
        var tval = $('textarea[name="resumo"]').val(),
                tlength = tval.length,
                set = 140,
                remain = parseInt(set - tlength);
        $('#caracteres').text(remain);
        if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
            $('textarea[name="resumo"]').val((tval).substring(0, tlength - 1))
        }
    });


});

function rawurlencode(str) {

    str = (str + '').toString();

    return encodeURIComponent(str)
            .replace(/!/g, '%21')
            .replace(/'/g, '%27')
            .replace(/\(/g, '%28')
            .replace(/\)/g, '%29')
            .replace(/\*/g, '%2A');
}