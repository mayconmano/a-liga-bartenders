$(document).ready(function(){
	
	window.ParsleyValidator.setLocale('pt');

	$('#form_question').parsley();

	tinymce.init({
        selector: "textarea",    
        width: 500,
        height: 300,        
        toolbar: "insertfile undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview fullpage | forecolor backcolor",     
    });

    App.wizard();

    var item = 1;
      var notAllow = [];    
      $('#wizard1').on('change', function(e, data) {

        if(data.step == 1){
          var validate_idiomas = false;
          $('.idiomas:checked').each(function(){
            validate_idiomas = true;   
          });
          if(!validate_idiomas) e.preventDefault();
        }     

        if(notAllow.indexOf(data.step) !== -1)
        {
          if (false === $('#form_question').parsley().validate('block' + data.step))
            e.preventDefault();  
        }    
              
        item = $('#wizard1').wizard('selectedItem').step;   
      });

      
      $('#wizard1').on('changed', function(e, data) {
        selectedItem = $('#wizard1').wizard('selectedItem');    
        if(notAllow.indexOf(selectedItem.step) == -1){
          if(item > selectedItem.step)
            $('#wizard1').wizard('previous');
          else
          {
            if(selectedItem.step != 5)
              $('#wizard1').wizard('next');
          }
        }

      });

      $('#wizard1').on('finished', function(e){    
        $('#form_question').submit();        
      });

      $('.idiomas').on('ifChecked', function(event){
        var val = $(this).val();
        $('.steps li').each(function(i,v){        
            if($(this).attr('idioma') == val) 
            {
              $(this).removeClass('hide'); 
              switch($(this).attr('idioma'))
              {
                case 'pt':
                  notAllow.push(2);              
                break;
                case 'en':
                  notAllow.push(3);              
                break;
                case 'es':
                  notAllow.push(4);        
                break;
              }          
            }
          });  
          notAllow.sort();
      });

      $('.idiomas').on('ifUnchecked', function(event){
         var val = $(this).val();
         $('.steps li').each(function(i, v){        
            if($(this).attr('idioma') == val) 
            {
              $(this).addClass('hide'); 
              switch($(this).attr('idioma'))
              {
                case 'pt':
                  index = notAllow.indexOf(2);
                  notAllow.splice(index,1);  
                break;
                case 'en':
                  index = notAllow.indexOf(3);
                  notAllow.splice(index,1);
                break;
                case 'es':              
                  index = notAllow.indexOf(4);
                  notAllow.splice(index,1);
                break;
              }  
            }
          });
          notAllow.sort(); 
      });

    $('.mais_resposta').click(function(){
        if($(this).attr('id') == 'pt'){
            resposta = 'resposta_pt[]';
        }else if($(this).attr('id') == 'en'){
            resposta = 'resposta_en[]';
        }else if($(this).attr('id') == 'es'){
            resposta = 'resposta_es[]';
        }
        if(resposta){
            $(this).parent().find('#respostas').append('<div class="input-group">'+
                       '<input type="text" name="'+resposta+'" class="form-control" required placeholder="Resposta" />   '+
                        '<span class="input-group-btn">'+
                        '<button class="btn btn-danger excluir_resposta" type="button">X</button>'+
                        '</span>'+
                      '</div>');
        }
    });

    $(document).on('click', '.excluir_resposta', function(){                          
        $(this).parent().parent().remove();
    });
 
});
