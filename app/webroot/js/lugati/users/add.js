$(document).ready(function(){
	
	window.ParsleyValidator.setLocale('pt');

	$('form').parsley();

	$('input[data-mask="telefone"]').mask('(99) 9999-9999?9');    

});