$(document).ready(function(){

	$(".multiselect").multiselect();
	$('form').submit(function(e){
		e.preventDefault();

		var top10 = new Array();

		$('#countries option:selected').each(function(i, v){				
			top10.push({'poie_id': $(this).val()});			
		});
		
		dados = JSON.stringify(top10);		

		$.ajax({
			url: 'http://'+window.location.host+config_cidade+'/top10/adicionar',
			data: {dados:dados},
			type: 'POST',
			dataType: 'json',
			success: function(data){
				if(data == 'sucesso')
				{					
			    	$.gritter.add({
			        	title: 'Sucesso',
			        	text: 'Top 10 alterado.',
			        	class_name: 'success'
			      	});				    
				}
				else
				{
					$.gritter.add({
				        title: 'Erro',
				        text: 'Não foi possivel alterar o Top 10, por favor tente mais tarde.',
				        class_name: 'danger'
				      });
				}
			}
		});

	});

});