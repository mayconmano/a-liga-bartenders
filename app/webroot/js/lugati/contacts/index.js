var functions = $('<div class="btn-group"><button class="btn btn-default btn-md" type="button">Ações</button><button data-toggle="dropdown" class="btn btn-md btn-primary dropdown-toggle" type="button"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul role="menu" class="dropdown-menu pull-right"><li><a href="" class="visualizar">Visualizar</a></li><li><a href="" class="deletar">Deletar</a></li></ul></div>');
$("#datatable tbody tr td:last-child").each(function(){
  $(this).html("");
  functions.clone().appendTo(this);
});


$(document).ready(function(){
  
  $('#datatable').dataTable();
  $('.dataTables_length select').addClass('form-control');     

  $(document).on('click', '.visualizar', function(e){
  	e.preventDefault();
  	var id = $(this).parents().eq(4).attr('id');
    window.location = 'http://'+window.location.host+config_cidade+'/contato/visualizar/'+id;	
  });

  $(document).on('click', '.deletar', function(e){
    e.preventDefault();
    var id = $(this).parents().eq(4).attr('id');
    window.location = 'http://'+window.location.host+config_cidade+'/contacts/delete/'+id;
  });  

});