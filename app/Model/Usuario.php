<?php

App::uses('AppModel', 'Model');

/**
 * Usuario Model
 *
 */
class Usuario extends AppModel {

    public function beforeSave($options = array()) {
        if (!empty($this->data[$this->alias]['senha'])) {
            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
            $this->data[$this->alias]['senha'] = $passwordHasher->hash(
                    $this->data[$this->alias]['senha']
            );
        }
        return true;
    }
    
    public $validate = array(
        'usuario' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo obrigatório.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'senha' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo obrigatório.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

}
