<?php

App::uses('AppModel', 'Model');

/**
 * Categoria Model
 *
 * @property Foto $Foto
 */
class Categoria extends AppModel {
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Foto' => array(
            'className' => 'Foto',
            'foreignKey' => 'categoria_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
