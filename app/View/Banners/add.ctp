

<div class="row">
    <div class="col-md-12">

        <div class="block-flat">
            <div class="header">							
                <h3>Adicionar Banner</h3>
            </div>
            <div class="content">
                <?php echo $this->Form->create('Banner', array('type' => 'file')); ?>
                <?php
                echo $this->Form->input('url', array('label' => 'Imagem', 'type' => 'file'));
                ?>
                <?php echo $this->Form->end(__('Adicionar')); ?>
            </div>
        </div>

    </div>
</div>
