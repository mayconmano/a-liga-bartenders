

<div class="row">
    <div class="col-md-12">

        <div class="block-flat">
            <div class="header">							
                <h3>Editar Banner</h3>
            </div>
            <div class="content">
                <?php echo $this->Form->create('Banner', array('type' => 'file')); ?>
                <?php
                echo $this->Form->input('id');
                echo $this->Form->input('url', array('label' => 'Imagem', 'type' => 'file'));
                ?>
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <?php echo '<img src="' . $this->data['Banner']['url'] . '" width="500">'; ?>
                    </div>
                </div>
                <?php echo $this->Form->end(__('Editar')); ?>
            </div>
        </div>

    </div>
</div>
