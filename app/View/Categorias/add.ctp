

<div class="row">
    <div class="col-md-12">

        <div class="block-flat">
            <div class="header">							
                <h3>Adicionar Categoria</h3>
            </div>
            <div class="content">
                <?php echo $this->Form->create('Categoria'); ?>
                <?php
                echo $this->Form->input('nome');
                ?>
                <?php echo $this->Form->end(__('Adicionar')); ?>
            </div>
        </div>

    </div>
</div>
