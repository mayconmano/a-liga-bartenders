

<div class="row">
    <div class="col-md-12">

        <div class="block-flat">
            <div class="header">							
                <h3>Editar Video</h3>
            </div>
            <div class="content">
                <?php echo $this->Form->create('Video', array('type' => 'file')); ?>
                <?php
                echo $this->Form->input('id');
                echo $this->Form->input('nome');
                echo $this->Form->input('url', array('type' => 'text'));
                echo $this->Form->input('imagem', array('type' => 'file'));
                ?>
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <?php echo '<img src="' . $this->data['Video']['imagem'] . '" width="150">'; ?>
                    </div>
                </div>
                <?php echo $this->Form->end(__('Editar')); ?>
            </div>
        </div>

    </div>
</div>
