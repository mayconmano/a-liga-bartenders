

<div class="row">
    <div class="col-md-12">

        <div class="block-flat">
            <div class="header">							
                <h3>Adicionar Video</h3>
            </div>
            <div class="content">
                <?php echo $this->Form->create('Video', array('type' => 'file')); ?>
                <?php
                echo $this->Form->input('nome');
                echo $this->Form->input('url', array('type' => 'text'));
                echo $this->Form->input('imagem', array('type' => 'file'));
                ?>
                <?php echo $this->Form->end(__('Adicionar')); ?>
            </div>
        </div>

    </div>
</div>
