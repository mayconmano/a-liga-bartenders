<div class="cl-sidebar">
    <div class="cl-toggle"><i class="fa fa-bars"></i></div>
    <div class="cl-navblock">
        <div class="menu-space">
            <div class="content">
                <div class="side-user">
                    <div class="avatar"></div>
                    <div class="info">
                        <a href="#">Administrador</a>            
                    </div>
                </div>
                <ul class="cl-vnavigation">                    
                    <li><a href="#"><i class="fa fa-location-arrow"></i><span>Usuários</span></a>
                        <ul class="sub-menu">
                            <li><a href="/gerenciador/usuarios"><i class="fa fa-angle-right"></i>Listar Usuários</a></li>
                            <li><a href="/gerenciador/usuarios/add"><i class="fa fa-angle-right"></i>Adicionar Usuários</a></li>              
                        </ul>
                    </li>  
                    <li><a href="#"><i class="fa fa-location-arrow"></i><span>Mensagens</span></a>
                        <ul class="sub-menu">
                            <li><a href="/gerenciador/mensagens"><i class="fa fa-angle-right"></i>Listar Mensagens</a></li>
                            <li><a href="/gerenciador/mensagens/add"><i class="fa fa-angle-right"></i>Adicionar Mensagens</a></li>              
                        </ul>
                    </li> 
                    <li><a href="#"><i class="fa fa-location-arrow"></i><span>Profissionais</span></a>
                        <ul class="sub-menu">
                            <li><a href="/gerenciador/profissionais"><i class="fa fa-angle-right"></i>Listar Profissionais</a></li>
                            <li><a href="/gerenciador/profissionais/add"><i class="fa fa-angle-right"></i>Adicionar Profissionais</a></li>              
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-location-arrow"></i><span>Fotos</span></a>
                        <ul class="sub-menu">
                            <li><a href="/gerenciador/categorias"><i class="fa fa-angle-right"></i>Listar Categorias</a></li>
                            <li><a href="/gerenciador/categorias/add"><i class="fa fa-angle-right"></i>Adicionar Categorias</a></li>                                         
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-location-arrow"></i><span>Videos</span></a>
                        <ul class="sub-menu">
                            <li><a href="/gerenciador/videos"><i class="fa fa-angle-right"></i>Listar Videos</a></li>
                            <li><a href="/gerenciador/videos/add"><i class="fa fa-angle-right"></i>Adicionar Videos</a></li>                                         
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-location-arrow"></i><span>Banner</span></a>
                        <ul class="sub-menu">
                            <li><a href="/gerenciador/banners"><i class="fa fa-angle-right"></i>Listar Banner</a></li>
                            <li><a href="/gerenciador/banners/add"><i class="fa fa-angle-right"></i>Adicionar Banner</a></li>                                         
                        </ul>
                    </li>
                </ul>                  
            </div>
        </div>
        <div class="text-right collapse-button" style="padding:7px 9px;">      
            <button id="sidebar-collapse" class="btn btn-default" style=""><i style="color:#fff;" class="fa fa-angle-left"></i></button>
        </div>
    </div>
</div>