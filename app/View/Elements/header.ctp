<!-- Fixed navbar -->
<div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="fa fa-gear"></span>
            </button>
            <a class="navbar-brand" href="/gerenciador">Gerenciador</a>
        </div>
        <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right user-nav">
                <li class="dropdown profile_menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Session->read('Auth.User.usuario'); ?><b class="caret"></b> <i class="fa fa-user"></i> </a>
                    <ul class="dropdown-menu">
                        <li><a href="/"><i class="fa fa-sign-in"></i> Acessar site</a></li>
                        <li class="divider"></li>
                        <li><a href="/usuarios/logout"><i class="fa fa-sign-out"></i> Sair</a></li>
                    </ul>
                </li>
            </ul>			

        </div><!--/.nav-collapse -->
    </div>
</div>