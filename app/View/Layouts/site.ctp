<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <title>A liga bartenders</title>

        <link href="/css/bootstrap.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Lato|Crete+Round' rel='stylesheet' type='text/css'>


        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link href="/css/lightbox.css" rel="stylesheet">
        <link href="/css/idangerous.swiper.css" rel="stylesheet">
        <link href="js/jquery.parsley/src/parsley.css" rel="stylesheet">

        <link href="/css/main.css"  rel="stylesheet">


        <script src="/js/jquery.js"></script>

    </head>
    <body data-spy="scroll" data-target="#navigation">

        <?php echo $this->fetch('content'); ?>

    </body>
</html>
