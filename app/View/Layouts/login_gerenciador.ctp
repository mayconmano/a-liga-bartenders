<!DOCTYPE html>
<html lang="pt">
    <head>
        <?php echo $this->Html->charset('utf-8'); ?> 	
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="images/favicon.png">

        <title>Sindicato Digitais</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

        <?php
        echo $this->Html->css('/js/bootstrap/dist/css/bootstrap');

        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('style');        
        
        ?>	

    </head>

    <body style="background-color: black;">

        <?php echo $this->fetch('content'); ?>

        <?php
        echo $this->Html->script('jquery');
        echo $this->Html->script('jquery.parsley/src/i18n/pt');
        echo $this->Html->script('jquery.parsley/dist/parsley.min');

        echo $this->Html->script('behaviour/general');

        echo $this->Html->script('behaviour/voice-commands');
        echo $this->Html->script('bootstrap/dist/js/bootstrap.min');
        echo $this->Html->script('jquery.flot/jquery.flot');
        echo $this->Html->script('jquery.flot/jquery.flot.pie');
        echo $this->Html->script('jquery.flot/jquery.flot.resize');
        echo $this->Html->script('jquery.flot/jquery.flot.labels');
        echo $this->Html->script('spin.min');
        echo $this->Html->script('login/funcoes');
        ?>

    </body>
</html>

