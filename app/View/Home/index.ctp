<div class="row">
    <div class="col-md-12">
        <div class="block-flat">
            <div class="header">							
                <h3><?php echo $this->Session->read('Auth.User.usuario'); ?> seja bem-vindo.</h3>
            </div>
            <div class="content">
                <br />
                <br />
                <p style="font-weight:600; font-size:14px;">
                    Você está acessando o gerenciador de conteudo do site A Liga Bartenders.
                </p>
                <p>Navegue por todo o sistema pelo menu ao lado esquerdo.</p>
            </div>
        </div>				
    </div>
</div>