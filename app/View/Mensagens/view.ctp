<div class="mensagens view">
<h2><?php echo __('Mensagen'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mensagen['Mensagen']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nome'); ?></dt>
		<dd>
			<?php echo h($mensagen['Mensagen']['nome']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Descricao'); ?></dt>
		<dd>
			<?php echo h($mensagen['Mensagen']['descricao']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mensagen'), array('action' => 'edit', $mensagen['Mensagen']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mensagen'), array('action' => 'delete', $mensagen['Mensagen']['id']), array(), __('Are you sure you want to delete # %s?', $mensagen['Mensagen']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mensagens'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mensagen'), array('action' => 'add')); ?> </li>
	</ul>
</div>
