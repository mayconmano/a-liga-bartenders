
<div class="row">
    <div class="col-md-12">
        <div class="block-flat">
            <div class="header">							
                <h3><?php echo __('Mensagens'); ?></h3>
            </div>
            <div class="content">
                <div class="table-responsive">
                    <table class="table table-bordered" id="datatable" >

                        <tbody>
                        </tbody>						


                    </table>							
                </div>
            </div>
        </div>				
    </div>
</div> 

<script>


    $(document).ready(function () {

        var columns = [];
        columns.push({"title": "id", "data": "Mensagen.id", "name": "Mensagen.id"});
        columns.push({"title": "nome", "data": "Mensagen.nome", "name": "Mensagen.nome"});
        columns.push({
            "title": "Ações",
            "targets": -1,
            "data": null,
            "defaultContent": '<div class="btn-group"><button class="btn btn-default btn-md" type="button">Ações</button><button data-toggle="dropdown" class="btn btn-md btn-primary dropdown-toggle" type="button"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul role="menu" class="dropdown-menu pull-right"><li><a href="" class="editar">Editar</a></li><li><a href="" class="deletar">Deletar</a></li></ul></div>'
        });
        console.log(columns);
        $('#datatable').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "http://" + window.location.host + "/gerenciador/mensagens/lista",
            "columns": columns

        });

        $('.dataTables_length select').addClass('form-control');

        $(document).on('click', '.editar', function (e) {
            e.preventDefault();
            var id = $(this).parents().eq(4).attr('id');
            window.location = 'http://' + window.location.host + '/gerenciador/mensagens/edit/' + id;
        });

        $(document).on('click', '.deletar', function (e) {
            e.preventDefault();
            var id = $(this).parents().eq(4).attr('id');
            window.location = 'http://' + window.location.host + '/gerenciador/mensagens/delete/' + id;
        });

        $(document).on('click', '.ativo', function (e) {
            e.preventDefault();
            var id = $(this).parent().parent().attr('id');
            var elemt = $(this);
            $.ajax({
                url: 'http://' + window.location.host + '/gerenciador/mensagens/status/' + id + '/1',
                dataType: 'json',
                success: function (data) {
                    elemt.removeClass('label-default ativo').addClass('label-success inativo').html('Ativo');
                    $.gritter.add({
                        title: data.title,
                        text: data.text,
                        class_name: data.class_name
                    });
                }
            });
        });

        $(document).on('click', '.inativo', function (e) {
            e.preventDefault();
            var id = $(this).parent().parent().attr('id');
            var elemt = $(this);
            $.ajax({
                url: 'http://' + window.location.host + '/gerenciador/mensagens/status/' + id + '/0',
                dataType: 'json',
                success: function (data) {
                    elemt.removeClass('label-success inativo').addClass('label-default ativo').html('Inativo');
                    $.gritter.add({
                        title: data.title,
                        text: data.text,
                        class_name: data.class_name
                    });
                }
            });
        });

    });
</script>