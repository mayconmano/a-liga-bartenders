

<div class="row">
    <div class="col-md-12">

        <div class="block-flat">
            <div class="header">							
                <h3>Adicionar Mensagen</h3>
            </div>
            <div class="content">
                <?php echo $this->Form->create('Mensagen'); ?>
                <?php
                echo $this->Form->input('nome');
                echo $this->Form->input('descricao');
                ?>
                <?php echo $this->Form->end(__('Adicionar')); ?>
            </div>
        </div>

    </div>
</div>
