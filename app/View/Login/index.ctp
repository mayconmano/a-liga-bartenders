<div id="cl-wrapper" class="login-container">

    <div class="middle-login">	
        <div class="block-flat">
            <div class="header">							
                <h3 class="text-center">
                    <?php echo $this->Html->image('logo.png', array('alt' => 'loho', 'class' => 'logo-img')); ?>
                </h3>
            </div>
            <div>
                <form action="/usuarios/login" method="post" style="margin-bottom: 0px !important;" class="form-horizontal" data-parsley-validate>
                    <div class="content">
                        <h4 class="title">Painel de controle</h4>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="data[Usuario][usuario]" parsley-trigger="change" required placeholder="Usuario" id="usuario" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="mensagemErro"></div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" name="data[Usuario][senha]" parsley-trigger="change" required placeholder="Senha" id="senha" class="form-control"> 
                                </div>
                            </div>
                        </div>	
                        <?php echo $this->Session->flash(); ?>						
                    </div>
                    <div class="foot">                        
                        <button class="btn btn-primary" data-dismiss="modal" type="submit">Entrar</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="text-center out-links"><a href="#">Desenvolvido por MSC Desenvolvimento e Soluções de Software. Todos os direitos reservados.</a></div>
    </div> 

</div>