

<div class="row">
    <div class="col-md-12">

        <div class="block-flat">
            <div class="header">							
                <h3>Editar Usuario</h3>
            </div>
            <div class="content">
                <?php echo $this->Form->create('Usuario'); ?>
                <?php
                echo $this->Form->input('id');
                echo $this->Form->input('usuario');
                echo $this->Form->input('senha', array('type' => 'password'));
                ?>
                <?php echo $this->Form->end(__('Editar')); ?>
            </div>
        </div>

    </div>
</div>
