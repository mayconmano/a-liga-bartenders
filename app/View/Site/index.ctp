<div id="mobile-slider" class="visible-xs">
    <p><img class="mobile-logo" src="images/logo-wht-mobile@2x.png" alt="logo-wht-mobile"  /><span class="nav-icon"><i class="fa fa-bars"></i></span></p>
</div>
<!-- ============================== HEADER/NAV =========================== -->

<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-2 col-sm-2 hidden-xs">
                <h1 id="logo" class="text-hide">A liga bartenders</h1>
            </div><!-- end col -->
            <div class="col-lg-8 col-md-10 col-sm-10">

                <nav id="navigation" class="navbar navbar-right" role="navigation">
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#home" title="Home">Home</a></li>
                            <li><a href="#about-us" title="#">Sobre</a></li>
                            <li><a href="#gallery" title="#">Fotos</a></li>
                            <li><a href="#posts" title="Blog">Videos</a></li>
                            <li><a href="#contact" title="Contact">Contato</a></li>
                        </ul>
                    </div><!-- emd collapse -->
                </nav>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</header>

<!-- ============================== CAROUSEL=========================== -->

<div id="main-page">
    <div id="home" class="home-carousel hidden-xs">
        <div id="myCarousel" class="carousel slide" style="background: url('<?= $banners['Banner']['url']; ?>') no-repeat fixed; background-size: cover;">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php foreach ($mensagens as $key => $mensagem) { ?>
                    <li data-target="#myCarousel" data-slide-to="<?= $key; ?>" class="<?= ($key == 0 ? 'active' : ''); ?>"></li>
                <?php } ?>                
            </ol>
            <div class="carousel-inner">

                <?php foreach ($mensagens as $key => $mensagem) { ?>
                    <div class="item <?= ($key == 0 ? 'active' : ''); ?>">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1><?= $mensagem['Mensagen']['nome'] ?></h1>
                                <p><?= $mensagem['Mensagen']['descricao'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div><!-- carousel -->
    </div><!-- end home -->
    <!-- ============================= ABOUT US ============================= -->

    <div id="about-wrap">
        <div class="container text-center" id="about-us">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <h1>Sobre</h1>
                    <p id="intro">A Liga Bartender's é uma empresa totalmente voltada para o ramo de animações de festas e coquetelaria.</p>
                </div><!-- end 12 -->
            </div><!-- end row -->

            <div class="row">
                <?php foreach ($profissionais as $profissionai) { ?>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="team">
                            <img class="img-responsive" src="<?= $profissionai['Profissionai']['url'] ?>" alt="<?= $profissionai['Profissionai']['nome'] ?>" />
                            <div class="roll">
                                <div class="sn-links">
                                    <?= (!empty($profissionai['Profissionai']['facebook']) ? '<a href="' . $profissionai['Profissionai']['facebook'] . '" title="Facebook" target="blank"><i class="fa fa-facebook-square"></i></a>' : '') ?>
                                    <?= (!empty($profissionai['Profissionai']['twitter']) ? '<a href="' . $profissionai['Profissionai']['twitter'] . '" title="Twitter" target="blank"><i class="fa fa-twitter-square"></i></a>' : '') ?>
                                    <?= (!empty($profissionai['Profissionai']['instagran']) ? '<a href="' . $profissionai['Profissionai']['instagran'] . '" title="Instagran" target="blank"><i class="fa fa-instagram"></i></a>' : '') ?>
                                </div><!-- end sn links -->
                            </div><!-- end roll -->
                        </div>
                        <h2>
                            <?= $profissionai['Profissionai']['nome'] ?></h2>
                        <p>
                            <?= $profissionai['Profissionai']['descricao'] ?>
                        </p>

                    </div>
                <?php } ?>

            </div><!-- end row -->

        </div><!-- end container -->
    </div><!-- end about wrapp -->
    <!-- ============================= GALLERY ============================= -->

    <div class="container" id="gallery">
        <div class="row">
            <div id="about" class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 text-center">
                <h1>Fotos</h1>
                <p>Confira abaixo algumas de nossas Fotos.</p>
            </div><!-- end 12 -->
        </div><!-- end row -->
    </div><!-- end container -->

    <div id="logos-wrapper">
        <ul class="portfolioFilter">
            <li class="filter btn" data-filter="all">Todos</li>
            <?php foreach ($categorias as $categoria) { ?>
                <li class="filter btn" data-filter="<?= $categoria['Categoria']['id']; ?>"><?= $categoria['Categoria']['nome']; ?></li>
            <?php } ?>
        </ul>

        <div id="Grid">

            <!-- item -->
            <?php foreach ($fotos as $foto) { ?>
                <div class="gal-item mix <?= $foto['filter']; ?>">
                    <img style="width: 360px; height: 250px;" class="img-responsive" src="<?= $foto['url']; ?>" alt="<?= $foto['nome']; ?>">
                    <div class="select"><a href="<?= $foto['url']; ?>" data-lightbox="image" title="<?= $foto['nome']; ?>"><i class="fa fa-search-plus"></i></a></div>
                </div>            
            <?php } ?>


        </div><!-- end grid -->
    </div><!-- end logos wrapper -->



    <!-- ========================= RECENT EXHIBITIONS ========================== -->

    <div  id="posts">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h1>Videos</h1>
                    <p>Confira abaixo alguns dos nossos Videos.</p>
                </div><!-- end 12 -->
            </div><!-- end row -->

            <div class="row">
                <!-- item -->
                <?php foreach ($videos as $video) { ?>    
                    <div class="item col-lg-4 col-md-4 col-sm-4">
                        <div class="item_wrap">
                            <img class="img-responsive" src="<?= $video['Video']['imagem']; ?>" alt="<?= $video['Video']['nome']; ?>">
                            <div class="select"><a href="JavaScript:html5Lightbox.showLightbox(3, '<?= $video['Video']['url']; ?>', '<?= $video['Video']['nome']; ?>');"><?= $video['Video']['nome']; ?></a></div>
                        </div>
                        <h2><?= $video['Video']['nome']; ?></h2>                            
                    </div>                
                <?php } ?>
            </div><!-- end row -->

        </div><!-- END CONTAINER -->
    </div><!-- end posts -->


    <!-- ============================= CONTACT ============================= -->

    <div class="image-lrg text-center" id="contact">
        <div class=c"ontainer">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <h1>Contato</h1>
                    <p>Faça um orçamento preenchendo o formulário abaixo!</p>
                </div>
            </div><!-- end row -->
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <form id="form_contato">                        
                        <p><input type="text" class="form-control" name="nome" placeholder="Nome" required></p>
                        <p><input type="email" class="form-control" name="email" placeholder="Email" required></p>
                        <p><input type="tel" class="form-control" name="telefone" placeholder="Telefone"></p>
                        <p><input type="text" class="form-control" name="data_evento" placeholder="Data" required></p>
                        <p><input type="text" class="form-control" name="local" placeholder="Local" required></p>
                        <p><input type="text" class="form-control" name="cidade" placeholder="Cidade" required></p>
                        <p><input type="text" class="form-control" name="n_convidados" placeholder="Numero de convidados" required></p>
                        <p><textarea class="form-control" name="mensagem" rows="6" placeholder="Mensagem" required></textarea></p>
                        <p id="mensagem"></p>
                        <p><button type="submit" class="submit btn btn-db">Enviar</button></p>                        
                    </form>
                </div><!-- end 8 -->
            </div><!-- end row -->
        </div><!-- end container -->

        <div class="map" style="width: 100%;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7046547.1778456345!2d-53.67045925000005!3d-30.416341350000007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9504720c40b45803%3A0xad9fb3dbaf9f73de!2sRio+Grande+do+Sul!5e0!3m2!1spt-BR!2sbr!4v1431042471703" width="800" height="600" frameborder="0" style="border:0"></iframe>
        </div>

    </div><!-- end contact -->


    <!-- ============================= FOOTER ============================== -->

</div>
<div class="modal"></div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/hoverIntent.js"></script>
<script src="js/holder.js"></script>
<script src="js/scripts.js"></script>
<script src="js/retina-1.1.0.min.js"></script>
<script src="js/lightbox-2.6.min.js"></script>
<script src="js/idangerous.swiper-2.1.min.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/html5lightbox.js"></script>

<script src="js/waypoints.min.js"></script>
<script src="js/swfobject.js"></script>

<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/jquery.parsley/src/i18n/pt.js"></script>
<script src="js/jquery.parsley/dist/parsley.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        window.ParsleyValidator.setLocale('pt');
        $('#form_contato').parsley();

        $('#form_contato').submit(function (e) {
            e.preventDefault();
            if ($(this).parsley().isValid()) {
                $.ajax({
                    url: '/site/form_contato',
                    data: $('#form_contato').serializeArray(),
                    type: 'POST',
                    beforeSend: function () {
                        $('body').addClass("loading");
                    },
                    success: function (retorno) {
                        if (retorno == 'sucesso') {
                            $('#form_contato').each(function () {
                                this.reset();
                            });
                            $('body').removeClass("loading");
                            $('#mensagem').html('<p style="color: #61c1b9;">Enviado com sucesso!</p>');
                        } else {
                            $('#form_contato').each(function () {
                                this.reset();
                            });
                            $('body').removeClass("loading");
                            $('#mensagem').html('<p style="color: red;">Falha no envio, por favor tente mais tarde.</p>');

                        }
                    }
                });
            } else {
                $('#mensagem').html('');
            }
        });

    });

</script>