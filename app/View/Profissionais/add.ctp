

<div class="row">
    <div class="col-md-12">

        <div class="block-flat">
            <div class="header">							
                <h3>Adicionar Profissionai</h3>
            </div>
            <div class="content">
                <?php echo $this->Form->create('Profissionai', array('type' => 'file')); ?>
                <?php
                echo $this->Form->input('nome');
                echo $this->Form->input('descricao');
                echo $this->Form->input('url', array('label' => 'Foto', 'type' => 'file'));
                echo $this->Form->input('facebook');
                echo $this->Form->input('twitter');
                echo $this->Form->input('instagran');
                ?>
                <?php echo $this->Form->end(__('Adicionar')); ?>
            </div>
        </div>

    </div>
</div>
