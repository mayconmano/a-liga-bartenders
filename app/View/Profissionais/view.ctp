<div class="profissionais view">
    <h2><?php echo __('Profissionai'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($profissionai['Profissionai']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Nome'); ?></dt>
        <dd>
            <?php echo h($profissionai['Profissionai']['nome']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Descricao'); ?></dt>
        <dd>
            <?php echo h($profissionai['Profissionai']['descricao']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Url'); ?></dt>
        <dd>
            <?php echo h($profissionai['Profissionai']['url']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Edit Profissionai'), array('action' => 'edit', $profissionai['Profissionai']['id'])); ?> </li>
        <li><?php echo $this->Form->postLink(__('Delete Profissionai'), array('action' => 'delete', $profissionai['Profissionai']['id']), array(), __('Are you sure you want to delete # %s?', $profissionai['Profissionai']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('List Profissionais'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Profissionai'), array('action' => 'add')); ?> </li>
    </ul>
</div>
