<?php

App::uses('AppController', 'Controller');

/**
 * Banners Controller
 *
 * @property Banner $Banner
 * @property PaginatorComponent $Paginator
 */
class BannersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $js = array();
    public $css = array();

    public function beforeRender() {
        $this->set('js', $this->js);
        $this->set('css', $this->css);
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {

        $this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

        $this->js[] = 'https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js';
        $this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
    }

    /**
     * lista method
     *
     * @return void
     */
    public function lista() {
        $this->render(false, false);
        $this->Banner->recursive = 1;
        $data = array();

        $search = "";

        if ($_GET['search']['value'] != "") {
            $colunasArr = array();
            foreach ($_GET['columns'] as $column) {
                if ($column['name'] != "") {
                    $colunasArr[] = $column['name'] . " LIKE '%" . $_GET['search']['value'] . "%'";
                }
            }
            $search = ' AND ' . join(' OR ', $colunasArr);
        }

        $ordem = "";
        if ($_GET['order'][0]['column'] != "") {
            $coluna = $_GET['columns'][$_GET['order'][0]['column']]['name'];
            $ordemTipo = $_GET['order'][0]['dir'];
            $ordem = " ORDER BY " . $coluna . " " . $ordemTipo;
        }

        $d = $this->Banner->find('all', array('conditions' => array('1=1 ' . $search . ' ' . $ordem . ' LIMIT ' . $_GET['start'] . ',' . $_GET['length'] . '')));
        $dTotal = $this->Banner->find('count');
        if (!empty($_GET['search']['value'])) {
            $dTotalFiltred = $this->Banner->find('count', array('conditions' => array('1=1 ' . $search)));
        } else {
            $dTotalFiltred = $dTotal;
        }


        $data['data'] = array();
        foreach ($d as $key => $v) {
            $data['data'][$key] = $v;
            $data['data'][$key]['DT_RowId'] = $v['Banner']['id'];
        }
        $data['recordsFiltered'] = $dTotalFiltred;
        $data['recordsTotal'] = $dTotal;
        echo json_encode($data);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Banner->exists($id)) {
            throw new NotFoundException(__('Invalid banner'));
        }
        $options = array('conditions' => array('Banner.' . $this->Banner->primaryKey => $id));
        $this->set('banner', $this->Banner->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->css[] = '/js/jquery.icheck/skins/square/blue';

        $this->js[] = 'jquery.icheck/icheck.min';

        if ($this->request->is('post')) {
            if (!empty($this->request->data['Banner']['url']['name'])) {
                $this->request->data['Banner']['url'] = $this->upload($this->request->data['Banner']['url']);
            } else {
                unset($this->request->data['Banner']['url']);
            }
            $this->Banner->create();
            if ($this->Banner->save($this->request->data)) {

                $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Cadastrado com sucesso!</strong></div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao cadastrar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->css[] = '/js/jquery.icheck/skins/square/blue';

        $this->js[] = 'jquery.icheck/icheck.min';

        if (!$this->Banner->exists($id)) {
            throw new NotFoundException(__('Invalid banner'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['Banner']['url']['name'])) {
                $this->request->data['Banner']['url'] = $this->upload($this->request->data['Banner']['url']);
            } else {
                unset($this->request->data['Banner']['url']);
            }
            if ($this->Banner->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Editado com sucesso!</strong></div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao editar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
            }
        } else {
            $options = array('conditions' => array('Banner.' . $this->Banner->primaryKey => $id));
            $this->request->data = $this->Banner->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Banner->id = $id;
        if (!$this->Banner->exists()) {
            throw new NotFoundException(__('Invalid banner'));
        }
        //$this->request->allowMethod('post', 'delete');
        if ($this->Banner->delete()) {

            $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Deletado com sucesso!</strong></div>'));
        } else {
            $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao deletar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * status method
     *
     * @param string $id
     * @param string $status
     * @return void
     */
    public function status($id, $status) {

        $this->autoRender = false;

        if ($status == 0 || $this->Banner->find('count', array('conditions' => array('status' => 1))) == 0) {

            $data['id'] = $id;
            $data['status'] = $status;

            if ($this->Banner->save($data)) {
                if ($status == 1) {
                    $return['title'] = 'Sucesso';
                    $return['text'] = 'Banner ativa!';
                    $return['class_name'] = 'success';
                } else {
                    $return['title'] = 'Sucesso';
                    $return['text'] = 'Banner inativa!';
                    $return['class_name'] = 'dark';
                }
            } else {
                $return['title'] = 'Erro';
                $return['text'] = 'Tente mais tarde!';
                $return['class_name'] = 'danger';
            }
        } else {
            $return['title'] = 'Erro';
            $return['text'] = 'Inative o banner ativo antes de ativar esse';
            $return['class_name'] = 'danger';
        }
        echo json_encode($return);
    }

}
