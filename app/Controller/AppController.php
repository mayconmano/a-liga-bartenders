<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
App::import('Vendor', 'phpmailer', array('file' => 'phpmailer/class.phpmailer.php'));

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Auth' => array(
            'loginAction' => array(
                'controller' => 'site',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'site',
                'action' => 'index'
            ),
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'Usuario',
                    'fields' => array(
                        'username' => 'usuario',
                        'password' => 'senha'
                    ),
                    'passwordHasher' => array(
                        'className' => 'Simple',
                        'hashType' => 'sha256'
                    )
                )
            )
        ),
        'Session'
    );

    public function enviarEmail($data, $mensagem = null) {

        $msg = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
        $msg .= "<html>";
        $msg .= "<head></head>";
        $msg .= "<body>";        
        $msg .= $mensagem;
        $msg .= "</body>";
        $msg .= "</html>";

        $mail = new PHPMailer();
        $mail->Mailer = "smtp";
        $mail->IsHTML(true);
        $mail->CharSet = "utf-8";
        $mail->SMTPSecure = "tls";
        //$mail->SMTPDebug  = 1;
        $mail->Host = "smtp.gmail.com";
        $mail->Port = "587";
        $mail->SMTPAuth = "true";
        $mail->Username = "msc.smtpauth2";
        $mail->Password = "netline1";

        $mail->From = 'msc.smtpauth2@gmail.com';
        $mail->FromName = 'A liga bartenders';

        $mail->AddAddress($data['email']);

        if (isset($data['copia_oculta'])) {
            $mail->AddBCC($data['copia_oculta']);
        }
        
        $mail->AddReplyTo($data['replayto']);

        $mail->Subject = $data['assunto'];

        $mail->Body = $msg;

        if (!$mail->Send()) {
            echo "erro";
        } else {
            echo "sucesso";
        }
    }

    public function upload($data) {
        $this->render(false, false);
        
        // Allowed extentions.
        $allowedExts = array("gif", "jpeg", "jpg", "png");

        // Get filename.
        $temp = explode(".", $data["name"]);

        // Get extension.
        $extension = end($temp);

        // An image check is being done in the editor but it is best to
        // check that again on the server side.
        // Do not use $_FILES["file"]["type"] as it can be easily forged.
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $data["tmp_name"]);

        if ((($mime == "image/gif") || ($mime == "image/jpeg") || ($mime == "image/pjpeg") || ($mime == "image/x-png") || ($mime == "image/png")) && in_array($extension, $allowedExts)) {
            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            // Save file in the uploads folder.
            move_uploaded_file($data["tmp_name"], getcwd() . "/uploads/" . $name);

            // Generate response.
            $response = new StdClass;
            $response->link = "/uploads/" . $name;
            return "/uploads/" . $name;
        }
    }
  

}
