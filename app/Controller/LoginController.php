<?php

App::uses('AppController', 'Controller');

class LoginController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(); // Libreando acesso a qualquer usuario
    }

    public function index() {
        $this->layout = 'login_gerenciador';
        if ($this->Auth->login()) {
            return $this->redirect(array('controller' => 'home'));
        }
    }

   

}

?>