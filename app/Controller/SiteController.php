<?php

App::uses('AppController', 'Controller');

class SiteController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    public function beforeRender() {
        parent::beforeRender();
    }

    public function index() {
        $this->layout = "site";
        $this->loadModel('Banner');
        $this->loadModel('Mensagen');
        $this->loadModel('Profissionai');
        $this->loadModel('Categoria');
        $this->loadModel('Video');

        $this->set('banners', $this->Banner->find('first', array('conditions' => array('status' => 1))));
        $this->set('mensagens', $this->Mensagen->find('all'));
        $this->set('profissionais', $this->Profissionai->find('all'));
        $this->set('categorias', $this->Categoria->find('all'));
        $this->set('fotos', $this->getImagens());
        $this->set('videos', $this->Video->find('all'));
    }

    public function getImagens() {

        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/app/webroot/files/';

        $result = array();
        if (file_exists($diretorio)) {
            $arquivos = scandir($diretorio);
            if (false !== $arquivos) {
                foreach ($arquivos as $arquivo) {
                    if ('.' != $arquivo && '..' != $arquivo && 'thumbnail' != $arquivo) {
                        $newArquivo = scandir($diretorio . $arquivo);
                        if (false !== $newArquivo) {
                            foreach ($newArquivo as $arquivo2) {
                                if ('.' != $arquivo2 && '..' != $arquivo2 && 'thumbnail' != $arquivo2) {
                                    $obj['url'] = '/files/' . $arquivo . '/' . $arquivo2;
                                    $obj['nome'] = $arquivo2;
                                    $obj['filter'] = $arquivo;
                                    $result[] = $obj;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function form_contato() {
        $this->render(false, false);
        
        $mensagem = '';
        $mensagem .= 'Nome: ' . $this->request->data['nome'];
        $mensagem .= '<br>Email: ' . $this->request->data['email'];
        (!empty($this->request->data['telefone']) ? $mensagem .= '<br>Telefone: ' . $this->request->data['telefone'] : '');
        $mensagem .= '<br>Data: ' . $this->request->data['data_evento'];
        $mensagem .= '<br>Local: ' . $this->request->data['local'];
        $mensagem .= '<br>Cidade: ' . $this->request->data['cidade'];
        $mensagem .= '<br>Numero de convidados: ' . $this->request->data['n_convidados'];
        $mensagem .= '<br><br>Mensagem: ' . $this->request->data['mensagem'];

        $data = array(
            'email' => 'aliga.bartenders@hotmail.com',            
            'assunto' => 'Site [Orçamento]',
            'copia_oculta' => 'concursosmaycon@gmail.com',
            'replayto' => $this->request->data['email']
        );
        
        return $this->enviarEmail($data, $mensagem);
    }

}

?>