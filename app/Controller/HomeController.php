<?php

App::uses('AppController', 'Controller');

/**
 * Sindicatos Controller
 *
 * @property Sindicato $Sindicato
 * @property PaginatorComponent $Paginator
 */
class HomeController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $js = array();
    public $css = array();

    public function beforeRender() {
        $this->set('js', $this->js);
        $this->set('css', $this->css);
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        
    }

}

?>