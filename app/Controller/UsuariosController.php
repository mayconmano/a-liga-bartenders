<?php

App::uses('AppController', 'Controller');

/**
 * Usuarios Controller
 *
 * @property Usuario $Usuario
 * @property PaginatorComponent $Paginator
 */
class UsuariosController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $js = array();
    public $css = array();

    public function beforeRender() {
        $this->set('js', $this->js);
        $this->set('css', $this->css);
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'logout', 'add');
    }

    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect('/gerenciador/home');
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><div class="icon"><i class="fa fa-times-circle"></i></div><strong>Erro!</strong> Login ou senha invalido(s).</div>'));
                return $this->redirect(array('controller' => 'login', 'action' => 'index'));
            }
            exit;
        }
    }

    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {

        $this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

        $this->js[] = 'https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js';
        $this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
    }

    /**
     * lista method
     *
     * @return void
     */
    public function lista() {
        $this->render(false, false);
        $this->Usuario->recursive = 1;
        $data = array();

        $search = "";

        if ($_GET['search']['value'] != "") {
            $colunasArr = array();
            foreach ($_GET['columns'] as $column) {
                if ($column['name'] != "") {
                    $colunasArr[] = $column['name'] . " LIKE '%" . $_GET['search']['value'] . "%'";
                }
            }
            $search = ' AND ' . join(' OR ', $colunasArr);
        }

        $ordem = "";
        if ($_GET['order'][0]['column'] != "") {
            $coluna = $_GET['columns'][$_GET['order'][0]['column']]['name'];
            $ordemTipo = $_GET['order'][0]['dir'];
            $ordem = " ORDER BY " . $coluna . " " . $ordemTipo;
        }

        $d = $this->Usuario->find('all', array('conditions' => array('1=1 ' . $search . ' ' . $ordem . ' LIMIT ' . $_GET['start'] . ',' . $_GET['length'] . '')));
        $dTotal = $this->Usuario->find('count');
        if (!empty($_GET['search']['value'])) {
            $dTotalFiltred = $this->Usuario->find('count', array('conditions' => array('1=1 ' . $search)));
        } else {
            $dTotalFiltred = $dTotal;
        }


        $data['data'] = array();
        foreach ($d as $key => $v) {
            $data['data'][$key] = $v;
            $data['data'][$key]['DT_RowId'] = $v['Usuario']['id'];
        }
        $data['recordsFiltered'] = $dTotalFiltred;
        $data['recordsTotal'] = $dTotal;
        echo json_encode($data);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Usuario->exists($id)) {
            throw new NotFoundException(__('Invalid usuario'));
        }
        $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
        $this->set('usuario', $this->Usuario->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->css[] = '/js/jquery.icheck/skins/square/blue';

        $this->js[] = 'jquery.icheck/icheck.min';

        if ($this->request->is('post')) {
            $this->Usuario->create();
            if ($this->Usuario->save($this->request->data)) {

                $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Cadastrado com sucesso!</strong></div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao cadastrar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->css[] = '/js/jquery.icheck/skins/square/blue';

        $this->js[] = 'jquery.icheck/icheck.min';

        if (!$this->Usuario->exists($id)) {
            throw new NotFoundException(__('Invalid usuario'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Usuario->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Editado com sucesso!</strong></div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao editar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
            }
        } else {
            $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
            $this->request->data = $this->Usuario->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Usuario->id = $id;
        if (!$this->Usuario->exists()) {
            throw new NotFoundException(__('Invalid usuario'));
        }
        //$this->request->allowMethod('post', 'delete');
        if ($this->Usuario->delete()) {

            $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Deletado com sucesso!</strong></div>'));
        } else {
            $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao deletar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * status method
     *
     * @param string $id
     * @param string $status
     * @return void
     */
    public function status($id, $status) {

        $this->autoRender = false;

        $data['id'] = $id;
        $data['status'] = $status;

        if ($this->Usuario->save($data)) {
            if ($status == 1) {
                $return['title'] = 'Sucesso';
                $return['text'] = 'Usuario ativa!';
                $return['class_name'] = 'success';
            } else {
                $return['title'] = 'Sucesso';
                $return['text'] = 'Usuario inativa!';
                $return['class_name'] = 'dark';
            }
        } else {
            $return['title'] = 'Erro';
            $return['text'] = 'Tente mais tarde!';
            $return['class_name'] = 'red';
        }

        echo json_encode($return);
    }

}
