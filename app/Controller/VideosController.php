<?php

App::uses('AppController', 'Controller');

/**
 * Videos Controller
 *
 * @property Video $Video
 * @property PaginatorComponent $Paginator
 */
class VideosController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $js = array();
    public $css = array();

    public function beforeRender() {
        $this->set('js', $this->js);
        $this->set('css', $this->css);
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {

        $this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

        $this->js[] = 'https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js';
        $this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
    }

    /**
     * lista method
     *
     * @return void
     */
    public function lista() {
        $this->render(false, false);
        $this->Video->recursive = 1;
        $data = array();

        $search = "";

        if ($_GET['search']['value'] != "") {
            $colunasArr = array();
            foreach ($_GET['columns'] as $column) {
                if ($column['name'] != "") {
                    $colunasArr[] = $column['name'] . " LIKE '%" . $_GET['search']['value'] . "%'";
                }
            }
            $search = ' AND ' . join(' OR ', $colunasArr);
        }

        $ordem = "";
        if ($_GET['order'][0]['column'] != "") {
            $coluna = $_GET['columns'][$_GET['order'][0]['column']]['name'];
            $ordemTipo = $_GET['order'][0]['dir'];
            $ordem = " ORDER BY " . $coluna . " " . $ordemTipo;
        }

        $d = $this->Video->find('all', array('conditions' => array('1=1 ' . $search . ' ' . $ordem . ' LIMIT ' . $_GET['start'] . ',' . $_GET['length'] . '')));
        $dTotal = $this->Video->find('count');
        if (!empty($_GET['search']['value'])) {
            $dTotalFiltred = $this->Video->find('count', array('conditions' => array('1=1 ' . $search)));
        } else {
            $dTotalFiltred = $dTotal;
        }


        $data['data'] = array();
        foreach ($d as $key => $v) {
            $data['data'][$key] = $v;
            $data['data'][$key]['DT_RowId'] = $v['Video']['id'];
        }
        $data['recordsFiltered'] = $dTotalFiltred;
        $data['recordsTotal'] = $dTotal;
        echo json_encode($data);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Video->exists($id)) {
            throw new NotFoundException(__('Invalid video'));
        }
        $options = array('conditions' => array('Video.' . $this->Video->primaryKey => $id));
        $this->set('video', $this->Video->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->css[] = '/js/jquery.icheck/skins/square/blue';

        $this->js[] = 'jquery.icheck/icheck.min';

        if ($this->request->is('post')) {
            if (!empty($this->request->data['Video']['imagem']['name'])) {
                $this->request->data['Video']['imagem'] = $this->upload($this->request->data['Video']['imagem']);
            } else {
                unset($this->request->data['Video']['imagem']);
            }
            $this->Video->create();
            if ($this->Video->save($this->request->data)) {

                $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Cadastrado com sucesso!</strong></div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao cadastrar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->css[] = '/js/jquery.icheck/skins/square/blue';

        $this->js[] = 'jquery.icheck/icheck.min';

        if (!$this->Video->exists($id)) {
            throw new NotFoundException(__('Invalid video'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['Video']['imagem']['name'])) {
                $this->request->data['Video']['imagem'] = $this->upload($this->request->data['Video']['imagem']);
            } else {
                unset($this->request->data['Video']['imagem']);
            }
            if ($this->Video->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Editado com sucesso!</strong></div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao editar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
            }
        } else {
            $options = array('conditions' => array('Video.' . $this->Video->primaryKey => $id));
            $this->request->data = $this->Video->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Video->id = $id;
        if (!$this->Video->exists()) {
            throw new NotFoundException(__('Invalid video'));
        }
        //$this->request->allowMethod('post', 'delete');
        if ($this->Video->delete()) {

            $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Deletado com sucesso!</strong></div>'));
        } else {
            $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao deletar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * status method
     *
     * @param string $id
     * @param string $status
     * @return void
     */
    public function status($id, $status) {

        $this->autoRender = false;

        $data['id'] = $id;
        $data['status'] = $status;

        if ($this->Video->save($data)) {
            if ($status == 1) {
                $return['title'] = 'Sucesso';
                $return['text'] = 'Video ativa!';
                $return['class_name'] = 'success';
            } else {
                $return['title'] = 'Sucesso';
                $return['text'] = 'Video inativa!';
                $return['class_name'] = 'dark';
            }
        } else {
            $return['title'] = 'Erro';
            $return['text'] = 'Tente mais tarde!';
            $return['class_name'] = 'red';
        }

        echo json_encode($return);
    }

}
