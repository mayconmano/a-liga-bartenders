<?php

App::uses('AppController', 'Controller');
App::import('Vendor', 'uploadHandler', array('file' => 'UploadHandler/UploadHandler.php'));

/**
 * Categorias Controller
 *
 * @property Categoria $Categoria
 * @property PaginatorComponent $Paginator
 */
class CategoriasController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $js = array();
    public $css = array();

    public function beforeRender() {
        $this->set('js', $this->js);
        $this->set('css', $this->css);
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {

        $this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

        $this->js[] = 'https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js';
        $this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
    }

    /**
     * lista method
     *
     * @return void
     */
    public function lista() {
        $this->render(false, false);
        $this->Categoria->recursive = 1;
        $data = array();

        $search = "";

        if ($_GET['search']['value'] != "") {
            $colunasArr = array();
            foreach ($_GET['columns'] as $column) {
                if ($column['name'] != "") {
                    $colunasArr[] = $column['name'] . " LIKE '%" . $_GET['search']['value'] . "%'";
                }
            }
            $search = ' AND ' . join(' OR ', $colunasArr);
        }

        $ordem = "";
        if ($_GET['order'][0]['column'] != "") {
            $coluna = $_GET['columns'][$_GET['order'][0]['column']]['name'];
            $ordemTipo = $_GET['order'][0]['dir'];
            $ordem = " ORDER BY " . $coluna . " " . $ordemTipo;
        }

        $d = $this->Categoria->find('all', array('conditions' => array('1=1 ' . $search . ' ' . $ordem . ' LIMIT ' . $_GET['start'] . ',' . $_GET['length'] . '')));
        $dTotal = $this->Categoria->find('count');
        if (!empty($_GET['search']['value'])) {
            $dTotalFiltred = $this->Categoria->find('count', array('conditions' => array('1=1 ' . $search)));
        } else {
            $dTotalFiltred = $dTotal;
        }


        $data['data'] = array();
        foreach ($d as $key => $v) {
            $data['data'][$key] = $v;
            $data['data'][$key]['DT_RowId'] = $v['Categoria']['id'];
        }
        $data['recordsFiltered'] = $dTotalFiltred;
        $data['recordsTotal'] = $dTotal;
        echo json_encode($data);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Categoria->exists($id)) {
            throw new NotFoundException(__('Invalid categoria'));
        }
        $options = array('conditions' => array('Categoria.' . $this->Categoria->primaryKey => $id));
        $this->set('categoria', $this->Categoria->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->css[] = '/js/jquery.icheck/skins/square/blue';

        $this->js[] = 'jquery.icheck/icheck.min';

        if ($this->request->is('post')) {
            $this->Categoria->create();            
            if ($this->Categoria->save($this->request->data)) {

                $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Cadastrado com sucesso!</strong></div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao cadastrar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->css[] = '/js/jquery.icheck/skins/square/blue';

        $this->js[] = 'jquery.icheck/icheck.min';

        if (!$this->Categoria->exists($id)) {
            throw new NotFoundException(__('Invalid categoria'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Categoria->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Editado com sucesso!</strong></div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao editar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
            }
        } else {
            $options = array('conditions' => array('Categoria.' . $this->Categoria->primaryKey => $id));
            $this->request->data = $this->Categoria->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Categoria->id = $id;
        if (!$this->Categoria->exists()) {
            throw new NotFoundException(__('Invalid categoria'));
        }
        //$this->request->allowMethod('post', 'delete');
        if ($this->Categoria->delete()) {

            $this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Deletado com sucesso!</strong></div>'));
        } else {
            $this->Session->setFlash(__('<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-remove"></i></div><strong>Erro ao deletar. Por favor tente novamente, caso continue com erro entre em contato com suporte!</strong></div>'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * status method
     *
     * @param string $id
     * @param string $status
     * @return void
     */
    public function status($id, $status) {

        $this->autoRender = false;

        $data['id'] = $id;
        $data['status'] = $status;

        if ($this->Categoria->save($data)) {
            if ($status == 1) {
                $return['title'] = 'Sucesso';
                $return['text'] = 'Categoria ativa!';
                $return['class_name'] = 'success';
            } else {
                $return['title'] = 'Sucesso';
                $return['text'] = 'Categoria inativa!';
                $return['class_name'] = 'dark';
            }
        } else {
            $return['title'] = 'Erro';
            $return['text'] = 'Tente mais tarde!';
            $return['class_name'] = 'red';
        }

        echo json_encode($return);
    }

    public function gerenciar_imagens($categoria_id = null) {

        $this->css[] = 'file-upload/blueimp-gallery.min';
        $this->css[] = 'file-upload/jquery.fileupload';
        $this->css[] = 'file-upload/jquery.fileupload-ui';
        $this->css[] = '/js/jquery.niftymodals/css/component';

        $this->js[] = 'fuelux/loader';
        $this->js[] = 'file-upload/jquery.ui.widget';
        $this->js[] = 'file-upload/tmpl.min';
        $this->js[] = 'file-upload/load-image.min';
        $this->js[] = 'file-upload/canvas-to-blob.min';
        $this->js[] = 'file-upload/jquery.blueimp-gallery.min';
        $this->js[] = 'file-upload/jquery.iframe-transport';
        $this->js[] = 'file-upload/jquery.fileupload';
        $this->js[] = 'file-upload/jquery.fileupload-process';
        $this->js[] = 'file-upload/jquery.fileupload-image';
        $this->js[] = 'file-upload/jquery.fileupload-validate';
        $this->js[] = 'file-upload/jquery.fileupload-ui';
        $this->js[] = 'jquery.niftymodals/js/jquery.modalEffects';

        $this->set('categoria', $this->Categoria->find('first', array('conditions' => array('id' => $categoria_id))));
        $this->set('categoria_id', $categoria_id);
    }


    public function uploadHandlerEdit($id) {

        $this->autoRender = false;
        
        $options = array(
            'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']) . '/files/'.$id.'/',
            'upload_url' => $_SERVER['SERVER_NAME'] . '/files/'.$id.'/'
        );

        $upload_handler = new UploadHandler($options);
    }

}
