<?php
/**
 * TelefonesSindicatoFixture
 *
 */
class TelefonesSindicatoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'telefone_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'sindicato_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_telefones_has_sindicatos_sindicatos1_idx' => array('column' => 'sindicato_id', 'unique' => 0),
			'fk_telefones_has_sindicatos_telefones1_idx' => array('column' => 'telefone_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'telefone_id' => 1,
			'sindicato_id' => 1
		),
	);

}
