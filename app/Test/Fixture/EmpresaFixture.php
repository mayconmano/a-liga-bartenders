<?php
/**
 * EmpresaFixture
 *
 */
class EmpresaFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'id_natureza_juridica_fk' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'id_subregiao_fk' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'id_regioes_fk' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'id_categorias_fk' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'id_redes_sociais_fk' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'cnpj' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'dt_abertura' => array('type' => 'date', 'null' => true, 'default' => null),
		'razao_social' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'nm_fantasia' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'qt_funcionarios' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'email' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'fl_ativo' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'dt_situacao' => array('type' => 'date', 'null' => true, 'default' => null),
		'fl_matriz' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'matricula' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'capital_registrado' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '11,4', 'unsigned' => false),
		'matricula_escritorio_contabil' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'qt_filiais' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_empresas_natureza_juridicas1_idx' => array('column' => 'id_natureza_juridica_fk', 'unique' => 0),
			'fk_regios_idx' => array('column' => 'id_regioes_fk', 'unique' => 0),
			'fk_subregioes_idx' => array('column' => 'id_subregiao_fk', 'unique' => 0),
			'fk_categorias_idx' => array('column' => 'id_categorias_fk', 'unique' => 0),
			'fk_redes_sociais_idx' => array('column' => 'id_redes_sociais_fk', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'id_natureza_juridica_fk' => 1,
			'id_subregiao_fk' => 1,
			'id_regioes_fk' => 1,
			'id_categorias_fk' => 1,
			'id_redes_sociais_fk' => 1,
			'cnpj' => 'Lorem ipsum dolor sit amet',
			'dt_abertura' => '2015-04-06',
			'razao_social' => 'Lorem ipsum dolor sit amet',
			'nm_fantasia' => 'Lorem ipsum dolor sit amet',
			'qt_funcionarios' => 1,
			'email' => 'Lorem ipsum dolor sit amet',
			'fl_ativo' => 1,
			'dt_situacao' => '2015-04-06',
			'fl_matriz' => 1,
			'matricula' => 1,
			'capital_registrado' => '',
			'matricula_escritorio_contabil' => 'Lorem ipsum dolor sit amet',
			'qt_filiais' => 1
		),
	);

}
