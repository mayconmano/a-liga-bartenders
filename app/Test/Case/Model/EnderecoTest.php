<?php
App::uses('Endereco', 'Model');

/**
 * Endereco Test Case
 *
 */
class EnderecoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.endereco',
		'app.cidade',
		'app.estado',
		'app.pessoa',
		'app.sindicato',
		'app.telefone',
		'app.tipo_telefone',
		'app.empresa',
		'app.empresas_telefone',
		'app.pessoas_telefone',
		'app.empresas_endereco'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Endereco = ClassRegistry::init('Endereco');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Endereco);

		parent::tearDown();
	}

}
