<?php
App::uses('Sindicato', 'Model');

/**
 * Sindicato Test Case
 *
 */
class SindicatoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sindicato',
		'app.telefone',
		'app.endereco'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Sindicato = ClassRegistry::init('Sindicato');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sindicato);

		parent::tearDown();
	}

}
