<?php
App::uses('EmpresasTelefone', 'Model');

/**
 * EmpresasTelefone Test Case
 *
 */
class EmpresasTelefoneTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.empresas_telefone',
		'app.empresa',
		'app.atividade',
		'app.empresas_atividade',
		'app.endereco',
		'app.empresas_endereco',
		'app.pessoa',
		'app.empresas_pessoa',
		'app.telefone',
		'app.tipo_telefone',
		'app.pessoas_telefone'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EmpresasTelefone = ClassRegistry::init('EmpresasTelefone');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EmpresasTelefone);

		parent::tearDown();
	}

}
