<?php
App::uses('NaturezaJuridica', 'Model');

/**
 * NaturezaJuridica Test Case
 *
 */
class NaturezaJuridicaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.natureza_juridica',
		'app.empresa'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->NaturezaJuridica = ClassRegistry::init('NaturezaJuridica');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->NaturezaJuridica);

		parent::tearDown();
	}

}
