<?php
App::uses('TipoTelefone', 'Model');

/**
 * TipoTelefone Test Case
 *
 */
class TipoTelefoneTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tipo_telefone',
		'app.telefone'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TipoTelefone = ClassRegistry::init('TipoTelefone');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TipoTelefone);

		parent::tearDown();
	}

}
