<?php
App::uses('Empresa', 'Model');

/**
 * Empresa Test Case
 *
 */
class EmpresaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.empresa',
		'app.atividade',
		'app.empresas_atividade',
		'app.endereco',
		'app.empresas_endereco',
		'app.pessoa',
		'app.empresas_pessoa',
		'app.telefone',
		'app.tipo_telefone',
		'app.empresas_telefone',
		'app.pessoas_telefone'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Empresa = ClassRegistry::init('Empresa');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Empresa);

		parent::tearDown();
	}

}
