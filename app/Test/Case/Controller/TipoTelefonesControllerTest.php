<?php
App::uses('TipoTelefonesController', 'Controller');

/**
 * TipoTelefonesController Test Case
 *
 */
class TipoTelefonesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tipo_telefone',
		'app.telefone'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testLista method
 *
 * @return void
 */
	public function testLista() {
		$this->markTestIncomplete('testLista not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

/**
 * testStatus method
 *
 * @return void
 */
	public function testStatus() {
		$this->markTestIncomplete('testStatus not implemented.');
	}

}
