<?php
App::uses('SindicatosController', 'Controller');

/**
 * SindicatosController Test Case
 *
 */
class SindicatosControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sindicato',
		'app.telefone',
		'app.tipo_telefone',
		'app.empresa',
		'app.empresas_telefone',
		'app.pessoa',
		'app.pessoas_telefone',
		'app.endereco'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testLista method
 *
 * @return void
 */
	public function testLista() {
		$this->markTestIncomplete('testLista not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

/**
 * testStatus method
 *
 * @return void
 */
	public function testStatus() {
		$this->markTestIncomplete('testStatus not implemented.');
	}

}
