<?php
App::uses('EmpresasController', 'Controller');

/**
 * EmpresasController Test Case
 *
 */
class EmpresasControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.empresa',
		'app.atividade',
		'app.empresas_atividade',
		'app.endereco',
		'app.empresas_endereco',
		'app.pessoa',
		'app.empresas_pessoa',
		'app.telefone',
		'app.tipo_telefone',
		'app.empresas_telefone',
		'app.pessoas_telefone'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testLista method
 *
 * @return void
 */
	public function testLista() {
		$this->markTestIncomplete('testLista not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

/**
 * testStatus method
 *
 * @return void
 */
	public function testStatus() {
		$this->markTestIncomplete('testStatus not implemented.');
	}

}
