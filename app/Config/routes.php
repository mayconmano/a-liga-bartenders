<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'site', 'action' => 'index'));

/* GERENCIADOR */
Router::connect('/gerenciador', array('controller' => 'login', 'action' => 'index'));
/* HOME */
Router::connect('/gerenciador/home', array('controller' => 'home', 'action' => 'index'));
/* USUARIOS */
Router::connect('/gerenciador/usuarios', array('controller' => 'usuarios', 'action' => 'index'));
Router::connect('/gerenciador/usuarios/lista', array('controller' => 'usuarios', 'action' => 'lista'));
Router::connect('/gerenciador/usuarios/add', array('controller' => 'usuarios', 'action' => 'add'));
Router::connect('/gerenciador/usuarios/edit/*', array('controller' => 'usuarios', 'action' => 'edit'));
Router::connect('/gerenciador/usuarios/delete/*', array('controller' => 'usuarios', 'action' => 'delete'));
/* MENSAGENS */
Router::connect('/gerenciador/mensagens', array('controller' => 'mensagens', 'action' => 'index'));
Router::connect('/gerenciador/mensagens/lista', array('controller' => 'mensagens', 'action' => 'lista'));
Router::connect('/gerenciador/mensagens/add', array('controller' => 'mensagens', 'action' => 'add'));
Router::connect('/gerenciador/mensagens/edit/*', array('controller' => 'mensagens', 'action' => 'edit'));
Router::connect('/gerenciador/mensagens/delete/*', array('controller' => 'mensagens', 'action' => 'delete'));
/* PROFISSIONAIS */
Router::connect('/gerenciador/profissionais', array('controller' => 'profissionais', 'action' => 'index'));
Router::connect('/gerenciador/profissionais/lista', array('controller' => 'profissionais', 'action' => 'lista'));
Router::connect('/gerenciador/profissionais/add', array('controller' => 'profissionais', 'action' => 'add'));
Router::connect('/gerenciador/profissionais/edit/*', array('controller' => 'profissionais', 'action' => 'edit'));
Router::connect('/gerenciador/profissionais/delete/*', array('controller' => 'profissionais', 'action' => 'delete'));
/* CATEGORIAS */
Router::connect('/gerenciador/categorias', array('controller' => 'categorias', 'action' => 'index'));
Router::connect('/gerenciador/categorias/lista', array('controller' => 'categorias', 'action' => 'lista'));
Router::connect('/gerenciador/categorias/add', array('controller' => 'categorias', 'action' => 'add'));
Router::connect('/gerenciador/categorias/edit/*', array('controller' => 'categorias', 'action' => 'edit'));
Router::connect('/gerenciador/categorias/delete/*', array('controller' => 'categorias', 'action' => 'delete'));
Router::connect('/gerenciador/categorias/gerenciar_imagens/*', array('controller' => 'categorias', 'action' => 'gerenciar_imagens'));
/* VIDEOS */
Router::connect('/gerenciador/videos', array('controller' => 'videos', 'action' => 'index'));
Router::connect('/gerenciador/videos/lista', array('controller' => 'videos', 'action' => 'lista'));
Router::connect('/gerenciador/videos/add', array('controller' => 'videos', 'action' => 'add'));
Router::connect('/gerenciador/videos/edit/*', array('controller' => 'videos', 'action' => 'edit'));
Router::connect('/gerenciador/videos/delete/*', array('controller' => 'videos', 'action' => 'delete'));
/* BANNERS */
Router::connect('/gerenciador/banners', array('controller' => 'banners', 'action' => 'index'));
Router::connect('/gerenciador/banners/lista', array('controller' => 'banners', 'action' => 'lista'));
Router::connect('/gerenciador/banners/add', array('controller' => 'banners', 'action' => 'add'));
Router::connect('/gerenciador/banners/edit/*', array('controller' => 'banners', 'action' => 'edit'));
Router::connect('/gerenciador/banners/delete/*', array('controller' => 'banners', 'action' => 'delete'));
Router::connect('/gerenciador/banners/status/*', array('controller' => 'banners', 'action' => 'status'));

/* SITE */
Router::connect('/', array('controller' => 'site', 'action' => 'index'));

/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
